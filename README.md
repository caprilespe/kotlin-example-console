# Description
This is a console kotlin project to understand and continue learning Kotlin ⚙️👨🏻‍💻

# Made with
[![Kotlin](https://img.shields.io/badge/kotlin-7f52ff?style=for-the-badge&logo=kotlin&logoColor=white&labelColor=000000)]()